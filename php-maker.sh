#!/bin/bash

#  _____ _____ _____ _____     _           
# |  _  |  |  |  _  |     |___| |_ ___ ___ 
# |   __|     |   __| | | | .'| '_| -_|  _|
# |__|  |__|__|__|  |_|_|_|__,|_,_|___|_|  
#
# Build PHP easily on leenoox!
#
# Author: Cerullo Davide
# homepage: http://pix3lworkshop.altervista.org/
#
# This script is provided "as is", without warranty, use it at your
# own risk.
#

### Changelog
#
# * PHP 8.0: XMLRPC extension is moved to PECL
# * PHP 8.0: JSON extension is always available

php_ver_min="8.0"
php_ver="8.0.19"
std_dir=`pwd`
src_dir="php-$php_ver"
architecture=`uname -m`
deps="libkrb5-dev libonig-dev libsodium-dev libxslt1-dev libzip-dev libxml2-dev libsqlite3-dev libbz2-dev libcurl4-gnutls-dev libenchant-dev libc-client2007e-dev"
pq_deps="libpq-dev"

PHP_API_VERSION=20200930    #TODO: Get it automatically (/main/php.h)
EXTENSION_DIR="EXTENSION_DIR=/usr/lib/php/$PHP_API_VERSION/"

default_configure="--prefix=/usr --host=$architecture-linux --with-config-file-path=/etc/php/$php_ver_min/cli/ --with-config-file-scan-dir=/etc/php/$php_ver_min/mods-available/ --with-openssl=shared --with-imap-ssl=shared --with-zlib=shared --enable-bcmath=shared --with-bz2=shared --enable-calendar=shared --with-curl=shared --enable-dba=shared --with-enchant=shared --with-ffi=shared --enable-ftp=shared --enable-gd=shared --with-webp --with-jpeg --with-xpm --with-freetype --with-gettext=shared --with-mhash=shared --with-imap=shared --with-kerberos=shared --enable-intl=shared --enable-mbstring=shared --with-sodium=shared --with-zip=shared --with-gnu-ld --with-pgsql=shared --with-pdo-pgsql=shared --with-sqlite3=shared --with-pdo-sqlite=shared --enable-ctype=shared --enable-exif=shared  --enable-fileinfo=shared --enable-pdo=shared --enable-phar=shared --enable-posix=shared --enable-soap=shared --enable-shmop=shared --enable-sockets=shared --enable-sysvmsg=shared --enable-sysvsem=shared --enable-sysvshm=shared --enable-tokenizer=shared --enable-dom=shared --enable-simplexml=shared  --enable-xml=shared --enable-xmlreader=shared --enable-xmlwriter=shared --with-xsl=shared --with-pdo-mysql=shared --with-mysqli=shared --enable-mysqlnd=shared --with-iconv=shared"
# -with-pdo-odbc=ibm-db2"

control()
{
CONTROL="Package: php$php_ver_min-%s
Version: %s
Architecture: amd64
Maintainer: Davide Cerullo
Depends: php$php_ver_min-%s
Section: php
Priority: optional
Description: %s module for PHP $php_ver_min
 This package provides a %s module for PHP $php_ver_min.
 .
 PHP (recursive acronym for PHP: Hypertext Preprocessor) is a widely-used
 open source general-purpose scripting language that is especially suited
 for web development and can be embedded into HTML.
 .
 This package is a dependency package, which depends on Debian's default
 PHP version (currently %s)."
    
PACKAGE="$1"

    printf "$CONTROL" "$PACKAGE" "$php_ver_min" "$PACKAGE" "$PACKAGE" "$PACKAGE" "$php_ver"
}

debian_bundler()
{
    ###
    ## Build core package
    ###
    
    mkdir -p "$std_dir/build/$src_dir/usr/bin/"
    mkdir -p "$std_dir/build/$src_dir/usr/lib/php/"
    
    bin=(php php-cgi phpdbg phar.phar php-config)
    for key in "${bin[@]}" ; do
        echo "copy ./tmp/usr/bin/$key => ./build/$src_dir/usr/bin/$key$php_ver_min"
        cp $std_dir/tmp/usr/bin/$key $std_dir/build/$src_dir/usr/bin/$key$php_ver_min
    done
    
    #fix file pointing
    sed -i 's/include_dir="${prefix}\/include\/php"/include_dir="${prefix}\/include\/php\/'$php_ver_min'"/' $std_dir/build/$src_dir/usr/bin/php-config$php_ver_min
    
    mkdir -p "$std_dir/build/$src_dir/DEBIAN/"
    getval=$(debian_info "core")
    echo "$getval" > "$std_dir/build/$src_dir/DEBIAN/control"
    
    make_dev    #make dev package
    
    ###
    ## End of core
    ###
    
    #Build extensions packages
    common=(bz2 calendar ctype exif fileinfo fileinfo ftp gettext iconv pdo phar posix shmop sockets sysvmsg sysvsem sysvshm tokenizer openssl)
    curl=(curl)
    xml=(xml dom simplexml xmlreader xmlwriter)
    pgsql=(pgsql pdo_pgsql)
    xsl=(xsl)
    #~ xmlrpc=(xmlrpc)
    zip=(zip)
    sodium=(sodium)
    bcmath=(bcmath)
    soap=(soap)
    sqlite3=(sqlite3 pdo_sqlite)
    #~ json=(json)
    intl=(intl)
    imap=(imap)
    mbstring=(mbstring)
    mysql=(mysqli mysqlnd)
    gd=(gd)
    #~ odbc=(odbc)

    keys=(common curl xml pgsql xsl zip sodium bcmath soap sqlite3 intl imap mbstring mysql gd) # odbc json xmlrpc)
    
    for thelist in "${keys[@]}" ; do
        reallist=$thelist[@]
        for key in "${!reallist}" ; do
        
            mkdir -p "$std_dir/build/$src_dir-$thelist/usr/lib/php/$PHP_API_VERSION/"
            echo "copy ./tmp/usr/lib/php/$PHP_API_VERSION/$key.so => ./build/$src_dir-$thelist/usr/lib/php/$PHP_API_VERSION"
            cp $std_dir/tmp/usr/lib/php/$PHP_API_VERSION/$key.so $std_dir/build/$src_dir-$thelist/usr/lib/php/$PHP_API_VERSION/
            
            #Debian
            mkdir -p "$std_dir/build/$src_dir-$thelist/DEBIAN/"
            getval=$(control $thelist)
            echo "$getval" > "$std_dir/build/$src_dir-$thelist/DEBIAN/control"
            
            if [ ! -d "$std_dir/build/$src_dir-$thelist/etc/php/$php_ver_min/mods-available/" ]; then
                mkdir -p "$std_dir/build/$src_dir-$thelist/etc/php/$php_ver_min/mods-available/"
            fi
        
            if [ ! -f "$std_dir/build/$src_dir-$thelist/etc/php/$php_ver_min/mods-available/$thelist" ]; then
                echo "; configuration for php $key module
; priority=20
extension=$key.so" > "$std_dir/build/$src_dir-$thelist/etc/php/$php_ver_min/mods-available/$key.ini"
        fi
            
        done
    done
    
    echo "File copy done!"
    
    #Build debian packages
    cd "$std_dir/build"
    for f in *; do
        if [ -d "$f" ]; then
            # $f is a directory
            #~ echo "$f"
            dpkg-deb --build "$f"
        fi
    done
}

bundler()
{
    ###
    ## Build core package
    ###
    
    mkdir -p "$std_dir/build/$src_dir/usr/bin/"
    mkdir -p "$std_dir/build/$src_dir/usr/lib/php/"
    
    bin=(php php-cgi phpdbg phar.phar php-config)
    for key in "${bin[@]}" ; do
        echo "copy ./tmp/usr/bin/$key => ./build/$src_dir/usr/bin/$key$php_ver_min"
        cp $std_dir/tmp/usr/bin/$key $std_dir/build/$src_dir/usr/bin/$key$php_ver_min
    done
    
    #fix file pointing
    sed -i 's/include_dir="${prefix}\/include\/php"/include_dir="${prefix}\/include\/php\/'$php_ver_min'"/' $std_dir/build/$src_dir/usr/bin/php-config$php_ver_min
    
    mkdir -p "$std_dir/build/$src_dir/DEBIAN/"
    getval=$(control "core")
    echo "$getval" > "$std_dir/build/$src_dir/DEBIAN/control"
    
    make_dev    #make dev package
    
    ###
    ## End of core
    ###
    
    #Build extensions packages
    common=(bz2 calendar ctype exif fileinfo fileinfo ftp gettext iconv pdo phar posix shmop sockets sysvmsg sysvsem sysvshm tokenizer openssl)
    curl=(curl)
    xml=(xml dom simplexml xmlreader xmlwriter)
    pgsql=(pgsql pdo_pgsql)
    xsl=(xsl)
    #~ xmlrpc=(xmlrpc)
    zip=(zip)
    sodium=(sodium)
    bcmath=(bcmath)
    soap=(soap)
    sqlite3=(sqlite3 pdo_sqlite)
    #~ json=(json)
    intl=(intl)
    imap=(imap)
    mbstring=(mbstring)
    mysql=(mysqli mysqlnd)
    gd=(gd)
    #~ odbc=(odbc)

    keys=(common curl xml pgsql xsl zip sodium bcmath soap sqlite3 intl imap mbstring mysql gd) # odbc json xmlrpc)
    
    for thelist in "${keys[@]}" ; do
        reallist=$thelist[@]
        for key in "${!reallist}" ; do
        
            mkdir -p "$std_dir/build/$src_dir-$thelist/usr/lib/php/$PHP_API_VERSION/"
            echo "copy ./tmp/usr/lib/php/$PHP_API_VERSION/$key.so => ./build/$src_dir-$thelist/usr/lib/php/$PHP_API_VERSION"
            cp $std_dir/tmp/usr/lib/php/$PHP_API_VERSION/$key.so $std_dir/build/$src_dir-$thelist/usr/lib/php/$PHP_API_VERSION/
            
            #Debian
            mkdir -p "$std_dir/build/$src_dir-$thelist/DEBIAN/"
            getval=$(control $thelist)
            echo "$getval" > "$std_dir/build/$src_dir-$thelist/DEBIAN/control"
            
            if [ ! -d "$std_dir/build/$src_dir-$thelist/etc/php/$php_ver_min/mods-available/" ]; then
                mkdir -p "$std_dir/build/$src_dir-$thelist/etc/php/$php_ver_min/mods-available/"
            fi
        
            if [ ! -f "$std_dir/build/$src_dir-$thelist/etc/php/$php_ver_min/mods-available/$thelist" ]; then
                echo "; configuration for php $key module
; priority=20
extension=$key.so" > "$std_dir/build/$src_dir-$thelist/etc/php/$php_ver_min/mods-available/$key.ini"
        fi
            
        done
    done
    
    echo "File copy done!"
    
    #Build debian packages
    cd "$std_dir/build"
    for f in *; do
        if [ -d "$f" ]; then
            # $f is a directory
            #~ echo "$f"
            dpkg-deb --build "$f"
        fi
    done
}

builder()
{
    cd $std_dir/$src_dir
    make

    if [ $? -eq 0 ]
    then
        mkdir "$std_dir/tmp"
        INSTALL_ROOT="$std_dir/tmp" make install
        
        bundler
        exit 0
    fi
}

set_phpize()
{
    #phpize
    echo "copy ./tmp/usr/bin/phpize => ./build/$src_dir-dev/usr/bin/phpize$php_ver_min"
    cp $std_dir/tmp/usr/bin/phpize $std_dir/build/$src_dir-dev/usr/bin/phpize$php_ver_min

    #fix file pointing
    sed -i 's/includedir="`eval echo ${prefix}\/include`\/php"/includedir="`eval echo ${prefix}\/include`\/php\/'$php_ver_min'"/' $std_dir/build/$src_dir-dev/usr/bin/phpize$php_ver_min
    
    sed -i 's/phpdir="`eval echo ${exec_prefix}\/lib\/php`\/build"/phpdir="`eval echo ${exec_prefix}\/lib\/php\/'$php_ver_min'`\/build"/' $std_dir/build/$src_dir-dev/usr/bin/phpize$php_ver_min
}

make_dev()
{
    mkdir -p "$std_dir/build/$src_dir-dev/usr/bin/"
    mkdir -p "$std_dir/build/$src_dir-dev/usr/lib/php/$php_ver_min"
    mkdir -p "$std_dir/build/$src_dir-dev/usr/include/php/"
    
    set_phpize
    
    #headers
    echo "copy ./tmp/usr/include/php => ./build/$src_dir-dev/usr/include/php/$php_ver_min/"
    cp -r $std_dir/tmp/usr/include/php $std_dir/build/$src_dir-dev/usr/include/php/
    mv $std_dir/build/$src_dir-dev/usr/include/php/php $std_dir/build/$src_dir-dev/usr/include/php/$php_ver_min

    #build
    echo "copy ./tmp/usr/lib/php/build => ./build/$src_dir-dev/usr/lib/php/build"
    cp -r $std_dir/tmp/usr/lib/php/build $std_dir/build/$src_dir-dev/usr/lib/php/$php_ver_min/
    
    mkdir -p "$std_dir/build/$src_dir-dev/DEBIAN/"
    getval=$(control "dev")
    echo "$getval" > "$std_dir/build/$src_dir-dev/DEBIAN/control"
}

configure()
{
    cd $src_dir
    export $EXTENSION_DIR
    ./configure $default_configure
    
    if [ $? -eq 0 ]
    then
        echo "Configure success! Now trying to make"
        
        #~ rm -rf "$std_dir/tmp"
        #~ rm -rf "$std_dir/build"
        
        builder
        exit 0
    else
        exit 1
    fi
}

echo "   _____ _____ _____ _____     _           
  |  _  |  |  |  _  |     |___| |_ ___ ___ 
  |   __|     |   __| | | | .'| '_| -_|  _|
  |__|  |__|__|__|  |_|_|_|__,|_,_|___|_|
  
Build PHP easily on leenoox!
";

case $1 in
  "builder") builder ;;
  "bundler") bundler ;;
  "dev") make_dev ;;
    *) 
    configure
    exit 0
esac
