#!/bin/bash

#  _____ _____ _____           _ _       _
# |  _  |  |  |  _  |___ _ _ _|_| |_ ___| |_ 
# |   __|     |   __|_ -| | | | |  _|  _|   |
# |__|  |__|__|__|  |___|_____|_|_| |___|_|_|
#
# Switch between PHP version easily!
#
# Author: Cerullo Davide
# homepage: http://pix3lworkshop.altervista.org/
#
# This script is provided "as is", without warranty, use it at your
# own risk.
#

CURRENT_8=8.0
CURRENT_7=7.4
CURRENT_5=5.0

function restartApache {
    
    if [ -f "/usr/bin/a2dismod" ]
    then
        a2dismod php$1
        a2enmod php$2
    fi

    APACHE=`which apache2`
    if [ -f ${APACHE} ]
    then
        service apache2 restart
    fi
}

function setVersion
{
    current="CURRENT_$1"
    current=${!current}
    path="/usr/bin/php$current"
    
    if [ -z $path ]
    then
        echo "PHP$current not found..."
    else
        echo "Switch to PHP$current..."

        update_alternatives $current

        echo "Done!"
    fi
}

function check_alternatives {
    [ -z `update-alternatives --list php | grep -F '$1'` ]
}

function update_alternatives {

    PHP=`which php$1`
    PHPCONFIG=`which php-config$1`
    PHPIZE=`which phpize$1`
    
    bin=(php php-config phpize)

    if [ -e "/usr/bin/update-alternatives" ]
    then

        echo "update-alternatives found..."

        for key in "${bin[@]}" ; do
        
            echo "Check if alternatives for $key exists..."
            
            if ! check_alternatives $key$1;
            then
                echo " Install new alternatives $key$1"
                update-alternatives --install /usr/bin/$key $key /usr/bin/$key$1 50
            else
                echo " Alternatives $key$1 already exists!"
            fi
            
            echo " Set /usr/bin/$key$1 as an alternative for $key..."
            update-alternatives --set $key /usr/bin/$key$1
        done
        
        echo "...update-alternatives done!"

    else
        echo "No update-alternatives command found, switching PHP executables manually"

        rm /usr/bin/php
        ln -s $PHP /usr/bin/php

        if [ ! -z "$PHPCONFIG" ]
        then
            rm /usr/bin/php-config
            ln -s $PHPCONFIG /usr/bin/php-config
        fi

        if [ ! -z "$PHPIZE" ]
        then
            rm /usr/bin/phpize
            ln -s $PHPIZE /usr/bin/phpize
        fi
    fi
}

echo " _____ _____ _____           _ _       _
|  _  |  |  |  _  |___ _ _ _|_| |_ ___| |_
|   __|     |   __|_ -| | | | |  _|  _|   |
|__|  |__|__|__|  |___|_____|_|_| |___|_|_|

Switch PHP version easily on leenoox!

Usage: phpswitcher {5|7}
";

if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root!" 1>&2
   exit 1
fi

case $1 in
  "5") setVersion $1 ;;
  "7") setVersion $1 ;;
  "8") setVersion $1 ;;
    *) echo $"Usage: $0 {5|7|8}"
    exit 1
esac
